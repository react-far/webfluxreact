import React, { useEffect, useState } from 'react'
import { combineValidators, isRequired } from 'revalidate'
import { Form as FinalForm, Field } from 'react-final-form'
import { Button, Form, Header } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import useFetchCustomer from '../../app/hooks/useFetchCustomer'
import ErrorMessage from '../form/ErrorMessage'
import TextInput from '../form/TextInput'

const validate = combineValidators({
  nombres: isRequired({ message: 'The name is required' }),
  apellidos: isRequired({ message: 'The lastName is required' }),
  fechaNac: isRequired({ message: 'The dob is required' }),
})

const CustomerForm = ({ customerId, submitHandler }) => {
  const [actionLabel, setActionLabel] = useState('Add Customer')
  // Custom hook
  const [customer, loading] = useFetchCustomer(customerId)

  useEffect(() => {
    if (customerId) {
      setActionLabel('Edit Customer')
    } else setActionLabel('Add Customer')
  }, [customerId])

  return (
    <FinalForm
      onSubmit={(values) => submitHandler(values)}
      initialValues={customerId && customer}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="pink" textAlign="center" />
          <Field name="nombres" component={TextInput} placeholder="Type the Name" />
          <Field name="apellidos" component={TextInput} placeholder="Type the Lastname" />
          <Field name="fechaNac" component={TextInput} type="date" placeholder="Select the dob" />
          {submitError && !dirtySinceLastSubmit && (
            <ErrorMessage error={submitError} text="Invalid username or password" />
          )}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="violet"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

CustomerForm.propTypes = {
  customerId: PropTypes.string,
  submitHandler: PropTypes.func.isRequired,
}

CustomerForm.defaultProps = {
  customerId: null,
}

export default CustomerForm
