// keys
export const TOKEN_KEY = 'token'

// endpoints
export const CUSTOMER_ENDPOINT = '/clientes'
export const AUTH_ENDPOINT = '/login'
export const DISHES_ENDPOINT = '/platos'
export const INVOICE_ENDPOINT = '/facturas'
