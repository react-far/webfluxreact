import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import { Breadcrumb, Container, Divider, Grid, Header, Icon, Label, Segment, Table } from 'semantic-ui-react'

import CustomerService from '../../app/api/customerService'
import DishService from '../../app/api/dishService'
import InvoiceService from '../../app/api/invoiceService'
import LoadingComponent from '../../components/common/LoadingComponent'

const InvoiceDetails = ({ match }) => {
  const [invoice, setInvoice] = useState(null)
  const [loading, setLoading] = useState(false)

  const fetchInvoice = useCallback(async () => {
    setLoading(true)
    try {
      const invoice = await InvoiceService.fetchInvoice(match.params.id)
      if (invoice) {
        const customer = await CustomerService.fetchCustomer(invoice.cliente.id)

        const items = []
        if (invoice.items.length > 0) {
          invoice.items.forEach((item) => {
            DishService.fetchDish(item.plato.id)
              .then((response) => {
                if (response) {
                  const dishItem = {
                    quantity: item.cantidad,
                    name: response.nombre,
                    price: response.precio,
                    id: response.id,
                    total: parseFloat(response.precio) * parseInt(item.cantidad, 0),
                  }
                  items.push(dishItem)
                }

                const totalAmount = items.reduce((sum, { total }) => sum + total, 0)

                const invoiceDetail = {
                  id: invoice.id,
                  description: invoice.descripcion,
                  observation: invoice.observacion,
                  customer,
                  items,
                  createdAt: new Date(invoice.creadoEn).toLocaleDateString(),
                  totalAmount,
                }
                setInvoice(invoiceDetail)
              })
              .catch((error) => toast.error(error))
          })
        }
      }
      setLoading(false)
    } catch (error) {
      setLoading(false)
      toast.error(error)
    }
  }, [match.params.id])

  useEffect(() => {
    fetchInvoice()
  }, [fetchInvoice])

  if (loading) return <LoadingComponent content="Loading Invoice Details..." />
  let invoiceDetailedArea = <h4>Invoice Details</h4>

  if (invoice) {
    invoiceDetailedArea = (
      <Segment.Group>
        <Segment>
          <Header as="h4" block color="violet">
            Customer
          </Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <p>
              <strong>Name: </strong>
              {`${invoice.customer.nombres} ${invoice.customer.apellidos}`}
            </p>
          </Segment>
        </Segment.Group>
        <Segment>
          <Header as="h4" block color="violet">
            Invoice
          </Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <p>
              <strong>Invoice Code: </strong>
              {invoice.id}
            </p>
            <p>
              <strong>Description: </strong>
              {invoice.description}
            </p>
            <p>
              <strong>Observation: </strong>
              {invoice.observation}
            </p>
            <p>
              <strong>Created At: </strong>
              {invoice.createdAt}
            </p>
          </Segment>
        </Segment.Group>
        <Segment>
          <Table celled striped color="violet">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="4">
                  <Icon name="food" />
                  Dishes
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Price (/S)</Table.HeaderCell>
                <Table.HeaderCell>Quantity</Table.HeaderCell>
                <Table.HeaderCell>Total (/S)</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {invoice.items.length > 0 &&
                invoice.items.map((item) => (
                  <Table.Row key={item.id}>
                    <Table.Cell>{item.name}</Table.Cell>
                    <Table.Cell>{+item.price.toFixed(2)}</Table.Cell>
                    <Table.Cell>{item.quantity}</Table.Cell>
                    <Table.Cell>{+item.total.toFixed(2)}</Table.Cell>
                  </Table.Row>
                ))}
            </Table.Body>
          </Table>
          <Container textAlign="right">
            <Label basic color="violet" size="large">
              Total Amount:
              <Label.Detail content={`S/${+invoice.totalAmount.toFixed(2)}`} />
            </Label>
          </Container>
        </Segment>
      </Segment.Group>
    )
  }

  return (
    <Segment>
      <Breadcrumb size="small">
        <Breadcrumb.Section>Invoice</Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section as={Link} to="/invoices">
          Invoice List
        </Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section active>Invoice Detail</Breadcrumb.Section>
      </Breadcrumb>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="address card outline" />
          Invoice Detail
        </Header>
      </Divider>
      <Container>
        <Grid columns="3">
          <Grid.Column width="3" />
          <Grid.Column width="10">{invoiceDetailedArea}</Grid.Column>
          <Grid.Column width="3" />
        </Grid>
      </Container>
    </Segment>
  )
}

InvoiceDetails.propTypes = {
  match: PropTypes.object.isRequired,
}

export default InvoiceDetails
